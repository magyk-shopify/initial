import { json, redirect } from "@remix-run/node";
import { Form, useLoaderData } from "@remix-run/react";
import { login } from "../../shopify.server";
import styles from "./styles.module.css";

export const loader = async ({ request }) => {
  const url = new URL(request.url);

  if (url.searchParams.get("shop")) {
    throw redirect(`/app?${url.searchParams.toString()}`);
  }

  return json({ showForm: Boolean(login) });
};

export default function App() {
  const { showForm } = useLoaderData();

  return (
    <button id="fetch-product">Fetch Product</button>
    <button id="fetch-products">Fetch All Products</button>
    <div id="product-info"></div>
    <div id="products-list"></div>

    <script>
        document.getElementById('fetch-product').addEventListener('click', () => {
            fetch('http://localhost:3000/product')
                .then(response => response.json())
                .then(data => {
                    const productInfo = document.getElementById('product-info');
                    productInfo.innerHTML = '';
                    const productElement = document.createElement('div');
                    const productName = document.createElement('h2');
                    productName.innerText = data.name;
                    const productDescription = document.createElement('div');
                    productDescription.innerHTML = data.description;
                    productElement.appendChild(productName);
                    productElement.appendChild(productDescription);
                    productInfo.appendChild(productElement);
                })
                .catch(error => console.error('Error fetching product:', error));
        });

        document.getElementById('fetch-products').addEventListener('click', () => {
            fetch('http://localhost:3000/products')
                .then(response => response.json())
                .then(data => {
                    const productsList = document.getElementById('products-list');
                    productsList.innerHTML = '';
                    data.forEach(product => {
                        const productElement = document.createElement('div');
                        const productName = document.createElement('h2');
                        productName.innerText = product.name;
                        const productDescription = document.createElement('div');
                        productDescription.innerHTML = product.description;
                        productElement.appendChild(productName);
                        productElement.appendChild(productDescription);
                        productsList.appendChild(productElement);
                    });
                })
                .catch(error => console.error('Error fetching products:', error));
        });
    </script>
  );
}
